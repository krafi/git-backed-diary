# pip install markdown2 pycryptodome PySide2
#
#from email.mime import audio
#from fileinput import filename
import re
import os
import sys
import json
import platform
import subprocess
from datetime import datetime
import shutil
from PySide6.QtWidgets import QApplication, QMainWindow, QHBoxLayout,QAbstractItemView,QMenu
from PySide6.QtWidgets import (
    QApplication, QWidget, QVBoxLayout, QHBoxLayout, QListWidget,
    QLineEdit, QPushButton, QCalendarWidget, QLabel, QMessageBox, QAbstractItemView
)
from PySide6.QtGui import QPalette, QColor, QFont
from PySide6.QtCore import Qt, QDate

from PySide6.QtWidgets import (QApplication, QMainWindow,  QVBoxLayout, QWidget,
                               QLineEdit, QCalendarWidget, QMessageBox, QLabel,
                               QPushButton, QListWidget, QInputDialog
                               )
from PySide6.QtGui import (QFont, QColor)
from PySide6.QtCore import Qt, QTimer, QDate


from dependencies.EmojiPicker import EmojiPicker
from dependencies.VarDataEncryptor import start_var_data_encryptor
from dependencies.FileEncryptor import start_FileEncryptor
from dependencies.HashPasswordAuthenticator import HashPasswdAuthenticator
from dependencies.GitSync import git_commands
from dependencies.JSON_Database import Json_Database_Initialization, Add_Page, modify_and_save_json, read_json, find_page_number, clear_and_input_new_data
from dependencies.file_manipulation_utils import (
    secure_delete_file,
    remove_files_with_extensions, secure_delete_file,
    copy_file, copy_file_to_directoryand_rename_file,
    copy_file_to_directoryand_rename_file,
    audiofile_to_flac_then_decode_then_enc,
    copy_file_to_directoryand_rename_audio,
    convert_markdown_to_css

)
from dependencies.Magic_Memory_Mark import MagicMemoryMarkTextEditor





def create_entry(The_new_file_name_text ,passwd):
    now = datetime.now()
    Photo_List = []
    Audio_List = []
    entry_date = now.strftime("%Y-%m-%d")
    entry_start_time = now.strftime("%H:%M:%S")
    entry_datetime = f"{entry_date} \n{entry_start_time}\n"  # new one
    Audio_List = []  # For json database
    write_path = (".enc.GitDiarySync/" + now.strftime("%b-%Y") + "/" + now.strftime("%d"))
    if not os.path.exists(write_path):
        os.makedirs(write_path) # just create the path

    def show_error_message(message):
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Warning)
        msg_box.setText(message)
        msg_box.setWindowTitle("Error")
        msg_box.exec_()

    try:
        saved_text = ""
    except AttributeError as e:
        show_error_message("Exit without saving new diary page!!")
        return

    join_date_and_note = f"\n{entry_datetime}\n{saved_text}"
    file_name_input = The_new_file_name_text

    sanitized_file_name = "".join(c if c.isalnum() else "-" for c in file_name_input.lower())
    filename = f"{entry_date}-{sanitized_file_name}.enc.GitDiarySync"
    Main_Name = sanitized_file_name
    entry_file_path = os.path.join(write_path, filename)
    write_path_Full_Path = entry_file_path
    write_path_Full_Path = write_path_Full_Path.replace("\\", "/")
    #Photo_List = Photo_List.replace("\\", "/")
    Database_Lieutenant_Path = write_path + '/' + 'Database_Lieutenant.json'
    if os.path.exists(Database_Lieutenant_Path):
        original_data = read_json(Database_Lieutenant_Path)
        check_int = 1
        while True:
            Total = str("Page" + str(check_int))
            if Total in original_data:
                check_int = check_int + 1
            else:
                break

        data = original_data
        Page_name = "Page" + str(check_int)
        Add_Page(data, Page_name,
                    Main_Name, write_path_Full_Path,
                    entry_date, entry_start_time,
                    Photo_List, Audio_List,
                    Database_Lieutenant_Path)

        modify_and_save_json(Database_Lieutenant_Path, original_data)

        print("JSON file modified and saved successfully.")

    else:
        Json_Database_Initialization(Main_Name, write_path_Full_Path,
                            entry_date, entry_start_time,
                            Photo_List, Audio_List,
                            Database_Lieutenant_Path)


    enc_note = start_var_data_encryptor("Enc_Fernet_PBKDF2_HMAC_SHA512", join_date_and_note, passwd)

    with open(entry_file_path, "w") as file:  # error
        file.write(enc_note)
    print(f"Diary entry saved to {entry_file_path}")
    remove_files_with_extensions()



#Sergeant < lieutenant < captain < major < colonel < general


def get_md_files_recursively(directory=".enc.GitDiarySync"):
    database_files = []
    for root, _, files in os.walk(directory):
        if "Database_Lieutenant.json" in files:
            database_files.append(os.path.join(root, "Database_Lieutenant.json"))

    full_paths = []
    for file_path in database_files:
        with open(file_path, 'r') as file:
            data = json.load(file)
            for page in data.values():
                full_paths.append(page["Main Full path"])

    if os.name == 'nt':  # Check if platform is Windows
        full_paths = [os.path.normpath(path).replace('/', '\\') for path in full_paths]

    return full_paths

def choose_file(passwd):
    global edit_mode
    edit_mode = False

    def on_toggled(checked, button):
        global edit_mode
        if checked:
            button_edit_toggle.setStyleSheet("background-color: #d9534f; color: white; font-size: 16px; padding: 10px;")
            button_edit_toggle.setText("Edit Mode: On")
            edit_mode = True
        else:
            button_edit_toggle.setStyleSheet("background-color: #5cb85c; color: white;")
            button_edit_toggle.setText("Edit Mode: Off")
            edit_mode = False

    def refresh_md_files():
        nonlocal md_files
        md_files = get_md_files_recursively()  # Re-fetch files

    def apply_theme(theme):
        if theme == "Peach":
            palette.setColor(QPalette.Window, QColor(255, 228, 181))
            palette.setColor(QPalette.WindowText, QColor(85, 85, 85))
            palette.setColor(QPalette.Base, QColor(255, 239, 213))
            palette.setColor(QPalette.AlternateBase, QColor(255, 228, 196))
            palette.setColor(QPalette.ToolTipBase, QColor(255, 245, 238))
            palette.setColor(QPalette.ToolTipText, QColor(85, 85, 85))
            palette.setColor(QPalette.Text, QColor(85, 85, 85))
            palette.setColor(QPalette.Button, QColor(255, 218, 185))
            palette.setColor(QPalette.ButtonText, QColor(85, 85, 85))
            palette.setColor(QPalette.Highlight, QColor(255, 182, 193))
            palette.setColor(QPalette.HighlightedText, QColor(85, 85, 85))

            button_Sync_With_Git.setStyleSheet("background-color: #FFDAC1; color: #555555; font-size: 16px; padding: 10px;")
            exit_button.setStyleSheet("background-color: #FFDAC1; color: #555555; font-size: 16px; padding: 10px;")
            button_Sync_With_Git.setStyleSheet("background-color: #FFDAC1; color: #555555; font-size: 16px; padding: 10px;")
            calendar_widget.setStyleSheet("background-color: #FFE4B5; color: #555555; border: 1px solid #DDC;")
            button_edit_toggle.setStyleSheet("background-color: #FFDAC1; color: #555555; font-size: 16px; padding: 10px;")
            button_create_file.setStyleSheet("background-color: #FFDAC1; color: #555555; font-size: 16px; padding: 10px;")
            select_button.setStyleSheet("background-color: #FFDAC1; color: #555555; font-size: 16px; padding: 10px;")
            list_widget.setStyleSheet("""
                QListWidget {
                    background-color: #FFF7EB;
                    color: #555555;
                    border: 1px solid #DDC;
                    font-size: 14px;
                }
                QListWidget::item:selected {
                    background-color: #FFDAB9;
                    color: #555555;
                }
            """)
        elif theme == "Light":
            palette.setColor(QPalette.Window, QColor(240, 240, 240))
            palette.setColor(QPalette.WindowText, QColor(0, 0, 0))
            palette.setColor(QPalette.Base, QColor(255, 255, 255))
            palette.setColor(QPalette.AlternateBase, QColor(240, 240, 240))
            palette.setColor(QPalette.ToolTipBase, QColor(255, 255, 255))
            palette.setColor(QPalette.ToolTipText, QColor(0, 0, 0))
            palette.setColor(QPalette.Text, QColor(0, 0, 0))
            palette.setColor(QPalette.Button, QColor(240, 240, 240))
            palette.setColor(QPalette.ButtonText, QColor(0, 0, 0))
            palette.setColor(QPalette.Highlight, QColor(0, 120, 215))
            palette.setColor(QPalette.HighlightedText, QColor(255, 255, 255))

            button_Sync_With_Git.setStyleSheet("background-color: #F0F0F0; color: #000000; font-size: 16px; padding: 10px;")
            exit_button.setStyleSheet("background-color: #F0F0F0; color: #000000; font-size: 16px; padding: 10px;")

            button_Sync_With_Git.setStyleSheet("background-color: #F0F0F0; color: #000000; font-size: 16px; padding: 10px;")
            calendar_widget.setStyleSheet("background-color: #F0F0F0; color: #000000; border: 1px solid #CCC;")
            button_edit_toggle.setStyleSheet("background-color: #F0F0F0; color: #000000; font-size: 16px; padding: 10px;")
            button_create_file.setStyleSheet("background-color: #F0F0F0; color: #000000; font-size: 16px; padding: 10px;")
            select_button.setStyleSheet("background-color: #F0F0F0; color: #000000; font-size: 16px; padding: 10px;")
            list_widget.setStyleSheet("""
                QListWidget {
                    background-color: #FFFFFF;
                    color: #000000;
                    border: 1px solid #CCC;
                    font-size: 14px;
                }
                QListWidget::item:selected {
                    background-color: #E0E0E0;
                    color: #000000;
                }
            """)
        elif theme == "Dark":
            palette.setColor(QPalette.Window, QColor(53, 53, 53))
            palette.setColor(QPalette.WindowText, Qt.white)
            palette.setColor(QPalette.Base, QColor(25, 25, 25))
            palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
            palette.setColor(QPalette.ToolTipBase, Qt.white)
            palette.setColor(QPalette.ToolTipText, Qt.white)
            palette.setColor(QPalette.Text, Qt.white)
            palette.setColor(QPalette.Button, QColor(53, 53, 53))
            palette.setColor(QPalette.ButtonText, Qt.white)
            palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
            palette.setColor(QPalette.HighlightedText, Qt.black)

            button_Sync_With_Git.setStyleSheet("background-color: #5bc0de; color: white; font-size: 16px; padding: 10px;")
            exit_button.setStyleSheet("background-color: #5bc0de; color: white; font-size: 16px; padding: 10px;")

            button_Sync_With_Git.setStyleSheet("background-color: #5bc0de; color: white; font-size: 16px; padding: 10px;")
            calendar_widget.setStyleSheet("background-color: #353535; color: white; border: 1px solid #888;")
            button_edit_toggle.setStyleSheet("background-color: #5cb85c; color: white; font-size: 16px; padding: 10px;")
            button_create_file.setStyleSheet("background-color: #0275d8; color: white; font-size: 16px; padding: 10px;")
            select_button.setStyleSheet("background-color: #5bc0de; color: white; font-size: 16px; padding: 10px;")
            list_widget.setStyleSheet("""
                QListWidget {
                    background-color: #2c2c2c;
                    color: white;
                    border: 1px solid #888;
                    font-size: 14px;
                }
                QListWidget::item:selected {
                    background-color: #565656;
                    color: #ffffff;
                }
            """)
        elif theme == "Dracula":
            palette.setColor(QPalette.Window, QColor(45, 52, 54))
            palette.setColor(QPalette.WindowText, QColor(238, 255, 255))
            palette.setColor(QPalette.Base, QColor(29, 31, 33))
            palette.setColor(QPalette.AlternateBase, QColor(45, 52, 54))
            palette.setColor(QPalette.ToolTipBase, QColor(238, 255, 255))
            palette.setColor(QPalette.ToolTipText, QColor(255, 121, 198))
            palette.setColor(QPalette.Text, QColor(238, 255, 255))
            palette.setColor(QPalette.Button, QColor(64, 76, 79))
            palette.setColor(QPalette.ButtonText, QColor(238, 255, 255))
            palette.setColor(QPalette.Highlight, QColor(255, 85, 85))
            palette.setColor(QPalette.HighlightedText, QColor(238, 255, 255))

            button_Sync_With_Git.setStyleSheet("background-color: #6272A4; color: #F8F8F2; font-size: 16px; padding: 10px;")
            exit_button.setStyleSheet("background-color: #6272A4; color: #F8F8F2; font-size: 16px; padding: 10px;")

            button_Sync_With_Git.setStyleSheet("background-color: #6272A4; color: #F8F8F2; font-size: 16px; padding: 10px;")
            calendar_widget.setStyleSheet("background-color: #2C2E32; color: #FF79C6; border: 1px solid #6272A4;")
            button_edit_toggle.setStyleSheet("background-color: #44475A; color: #F8F8F2; font-size: 16px; padding: 10px;")
            button_create_file.setStyleSheet("background-color: #6272A4; color: #F8F8F2; font-size: 16px; padding: 10px;")
            select_button.setStyleSheet("background-color: #BD93F9; color: #F8F8F2; font-size: 16px; padding: 10px;")
            list_widget.setStyleSheet("""
                QListWidget {
                    background-color: #282A36;
                    color: #F8F8F2;
                    border: 1px solid #6272A4;
                    font-size: 14px;
                }
                QListWidget::item:selected {
                    background-color: #44475A;
                    color: #F8F8F2;
                }
            """)

        app.setPalette(palette)

    md_files = get_md_files_recursively()
    file_dict = {}

    app.setStyle('Fusion')

    palette = QPalette()

    main_menu = QWidget()
    main_menu.setWindowTitle("File Selector")
    main_menu.setGeometry(100, 100, 900, 600)

    main_layout = QVBoxLayout(main_menu)
    main_layout.setSpacing(20)

    calendar_layout = QVBoxLayout()
    calendar_label = QLabel("Select a Date:")
    calendar_label.setStyleSheet("font-size: 16px; font-weight: bold; color: #f0f0f0;")
    calendar_widget = QCalendarWidget()
    calendar_widget.setGridVisible(True)
    calendar_layout.addWidget(calendar_label)
    calendar_layout.addWidget(calendar_widget)
    main_layout.addLayout(calendar_layout)

    theme_button = QPushButton("Toggle Theme")
    theme_button.setStyleSheet("background-color: #0275d8; color: white; font-size: 16px; padding: 10px;")
    theme_menu = QMenu(theme_button)

    theme_menu.addAction("Peach", lambda: apply_theme("Peach"))
    theme_menu.addAction("Light Mode", lambda: apply_theme("Light"))
    theme_menu.addAction("Dark Mode", lambda: apply_theme("Dark"))
    theme_menu.addAction("Dracula Mode", lambda: apply_theme("Dracula"))

    theme_button.setMenu(theme_menu)
    main_layout.addWidget(theme_button)

    middle_layout = QHBoxLayout()
    main_layout.addLayout(middle_layout)

    list_widget = QListWidget()
    list_widget.setFixedWidth(350)
    list_widget.setFixedHeight(400)
    middle_layout.addWidget(list_widget)

    controls_layout = QVBoxLayout()
    controls_layout.setAlignment(Qt.AlignTop)
    middle_layout.addLayout(controls_layout)

    button_edit_toggle = QPushButton('Edit Mode: Off', main_menu)
    button_edit_toggle.setCheckable(True)
    controls_layout.addWidget(button_edit_toggle)
    button_edit_toggle.toggled.connect(lambda checked: on_toggled(checked, button_edit_toggle))

    The_new_file_name = QLineEdit()
    The_new_file_name.setPlaceholderText("Enter new file name")
    controls_layout.addWidget(The_new_file_name)

    button_create_file = QPushButton("Create New File")
    controls_layout.addWidget(button_create_file)

    button_Sync_With_Git = QPushButton("Sync With Git")
    controls_layout.addWidget(button_Sync_With_Git)


    button_Sync_With_Git.clicked.connect(commit_to_git)



    exit_button = QPushButton("Exit")
    controls_layout.addWidget(exit_button)
    exit_button.clicked.connect(lambda: sys.exit(0))

    def refresh_widgets():
        refresh_md_files()
        list_widget.clear()
        file_dict.clear()

        for file in md_files:
            filename = os.path.basename(file)
            if file.endswith('.enc.GitDiarySync'):
                list_widget.addItem(filename)
                file_dict[filename] = file

        highlight_calendar_dates()

    def highlight_calendar_dates():
        highlighted_dates = []
        for file in md_files:
            filename = os.path.basename(file)
            if filename.endswith('.enc.GitDiarySync'):
                date_str = filename[:10]  # Assuming date is at the start
                year, month, day = map(int, date_str.split('-'))
                highlighted_dates.append(QDate(year, month, day))

        format_calendar_dates(highlighted_dates)

    def format_calendar_dates(dates):
        semiTransparentYellows = [
            QColor(255, 255, 0, 51),
            QColor(255, 255, 0, 102),
            QColor(255, 255, 0, 153),
            QColor(255, 255, 0, 204),
            QColor(255, 255, 0, 255)
        ]

        for date in dates:
            date_format = calendar_widget.dateTextFormat(date)
            current_color = date_format.background().color().rgba()
            for i, color in enumerate(semiTransparentYellows):
                if current_color == color.rgba():
                    next_index = (i + 1) % len(semiTransparentYellows)
                    date_format.setBackground(semiTransparentYellows[next_index])
                    break
            else:
                date_format.setBackground(semiTransparentYellows[0])

            calendar_widget.setDateTextFormat(date, date_format)

    def filter_files_by_date(selected_date):
        year = selected_date.year()
        month = selected_date.month()
        day = selected_date.day()
        date_prefix = f"{year}-{month:02d}-{day:02d}"

        list_widget.clear()
        for file in md_files:
            filename = os.path.basename(file)
            if filename.startswith(date_prefix) and filename.endswith('.enc.GitDiarySync'):
                list_widget.addItem(filename)
                file_dict[filename] = file

    calendar_widget.clicked.connect(filter_files_by_date)

    def set_the_new_name():
        new_file_name = The_new_file_name.text()
        if new_file_name:
            create_entry(new_file_name, passwd)
            refresh_widgets()

    button_create_file.clicked.connect(set_the_new_name)

    selected_file = None

    def on_select():
        nonlocal selected_file
        try:
            selected_file_name = list_widget.selectedItems()[0].text()
            selected_file = file_dict.get(selected_file_name, None)
        except IndexError:
            QMessageBox.warning(main_menu, "Warning", "No item selected.")
            return

        if selected_file is None:
            QMessageBox.warning(main_menu, "Warning", "File not found.")
        else:
            main_menu.close()

    select_button = QPushButton("Select")
    main_layout.addWidget(select_button)
    select_button.clicked.connect(on_select)

    refresh_widgets()

    apply_theme("Peach")  # Set Peach theme as default

    main_menu.setLayout(main_layout)
    main_menu.show()
    app.exec()

    return selected_file



def edit_n_view_mode(passwd):



    selected_file = choose_file(passwd)

    if selected_file:  # text file
        now = datetime.now()
        edit_start_time = now.strftime("%H:%M:%S")

        print("the selected file", selected_file)
        file_dir = os.path.dirname(selected_file)  # text file location
        all_files = os.listdir(file_dir)  # all other files like image
        matching_files = [file for file in all_files if file.endswith(".enc.img.GitDiarySync")]  # list only enc image

        for file in matching_files:  # enc image to normal image for view
            enc_file = (os.path.join(file_dir, file))
            remove_enc_ext = enc_file.replace(".enc.img.GitDiarySync", "")
            start_FileEncryptor("decrypt_file", enc_file, remove_enc_ext, passwd)


        temp_decrypted_file = ".tmp"
        copy_file(selected_file, temp_decrypted_file)
        with open(temp_decrypted_file, "r") as file:
            bytes_data = file.read()
        bytes_data_to_str = start_var_data_encryptor("Dec_Fernet_PBKDF2_HMAC_SHA512", bytes_data, passwd)
        print(edit_mode)
        if not edit_mode:  # view_mode
            read_only_warning = QMessageBox()
            read_only_warning.setWindowTitle("Read-Only Mode")
            read_only_warning.setIcon(QMessageBox.Warning)
            read_only_warning.setText(
                "Note: You have opened this file in read-only mode. Any attempt to save will not be successful.")
            read_only_warning.exec()
        if edit_mode:  # edit_mode
            read_only_warning = QMessageBox()
            read_only_warning.setWindowTitle("Read-Only Mode")
            read_only_warning.setIcon(QMessageBox.Warning)
            read_only_warning.setText(
                "Note: You have opened this file in Edit mode. Attempt to save.")
            read_only_warning.exec()
        filetoeditor = MagicMemoryMarkTextEditor(passwd, selected_file)
        filetoeditor.var_to_editor(bytes_data_to_str)
        filetoeditor.show()
        app.exec()


        dec_note = filetoeditor
        the_saved_text = dec_note.save_text()
        print("+++++++++++++++++++++++++++++=")
        print(the_saved_text)
        Photo_List = []
        Audio_List = []
        md_audio_pattern = re.compile(r'\[.*\]\((.*?)/([^/]+\.(mp3|wav|ogg|aac|m4a|flac|wma|aiff|alac|ape|opus|mid|midi))\)')
        matchs = md_audio_pattern.findall(the_saved_text)
        for match in matchs:  # Audio detector
            if match:
                print("match ", match)
                file_location = match[0]  # pure name without
                file_name = match[1]  # ext

                current_time = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                AudioFileDirLocation = f"{file_location}/{file_name}"
                AudioFileDirLocation = AudioFileDirLocation.lstrip()  #  remove space

                pure_file_name, file_extension = file_name.rsplit('.', 1)   #  split filename and extensions for changing filename
                new_file_name = f"{pure_file_name}-{current_time}.{file_extension}"
                new_file_name = new_file_name + ".enc.flac.GitDiarySync"

                encrypted_data = audiofile_to_flac_then_decode_then_enc(AudioFileDirLocation, passwd)

                with open("encrypted_img.tmp", "w") as file:
                    file.write(encrypted_data)

                audio_file_path = "encrypted_img.tmp"
                write_path_and_newname = file_dir

                max_file_size = 49 * 1024 * 1024

                if os.path.getsize(audio_file_path) > max_file_size:
                    os.remove(audio_file_path)
                    warning_message = (
                        "Alert: The audio file cannot be added because its size exceeds 49MB. "
                        "Please remove the corresponding URL markdown in edit mode. "
                        "This file will not be included in Database_Lieutenant.json, "
                        "So , The audio GUI will not display it."
                    )
                    QMessageBox.warning(None, "Warning",warning_message )
                else:
                    #shutil.move(audio_file_path, write_path_and_newname)

                    write_path_and_newname = file_dir + "/" + (file_name)

                    if platform.system() == "Windows":
                        abs_source_path = os.path.abspath("encrypted_img.tmp")
                        abs_dest_path = os.path.abspath(write_path_and_newname)
                        try:
                            if os.path.exists(abs_source_path):
                                print(abs_source_path, abs_dest_path)
                                shutil.move(abs_source_path, abs_dest_path)
                                print("File moved successfully.")
                            else:
                                print("Source file does not exist.")
                        except Exception as e:
                            print(f"Error: {e}")
                    else: # linux
                        shutil.move("encrypted_img.tmp", write_path_and_newname )

                    write_path_and_newname = (file_dir) + "/" + (file_name)
                    write_path_and_newname = (write_path_and_newname.replace("\\", "/"))


                    print("------------>>?", the_saved_text)
                    #  "Now, let's modify our entire text, basically replacing."
                    print("< ",(match[0]),(match[1]), ">")
                    escape = (match[0]) + r"/" + (match[1])
                    print("escape",escape) # C:/Users/88019/Downloads/asdasd.wav
                    md_pattern = re.escape(escape) # escape special characters in a string
                    print("write_path_and_newname > ",write_path_and_newname)
                    Audio_List.append(write_path_and_newname)
                    the_saved_text = re.sub(md_pattern, f"{write_path_and_newname}", the_saved_text)
                    print("+++++++++++++")
                    the_saved_text = (the_saved_text.replace("\\", "/")) # # # #


            else:
                QMessageBox.warning(None, "Warning", "No audio found")
                ##############################
    ################################## If audio added move it to correct path
        md_audio_pattern = re.compile(r'\[.*\]\((.*?)/([^/]+\.(enc\.flac\.GitDiarySync))\)')
        matchs = md_audio_pattern.findall(the_saved_text)
        for match in matchs:  # Audio detector
            if match:
                file_location = match[0]  # pure name without
                file_name = match[1]  # ext

                AudioDirLocation = f"{file_location}/{file_name}"
                AudioDirLocation = AudioDirLocation.lstrip()  # remove space

                write_path = os.path.dirname(selected_file)
                file_name = os.path.basename(selected_file)
                basenameofaudio = os.path.basename(AudioDirLocation)

                if AudioDirLocation.startswith(".enc.GitDiarySync/"): # Dont do anything with old one
                    Audio_List.append(AudioDirLocation)
                else:

                    shutil.move(AudioDirLocation, write_path)
                    final_filepath = f'{write_path}{AudioDirLocation[1:]}'

                    final_filepath = final_filepath.replace("\\", "/")
                    #final_filepath = copy_file_to_directoryand_rename_audio(AudioDirLocation, write_path, basenameofaudio)
                    print("final path ", final_filepath)
                    Audio_List.append(final_filepath)

                    #  "Now, let's modify our entire text, basically replacing."
                    md_pattern = re.escape(match[0]) + r"/" + re.escape(match[1])

                    the_saved_text = re.sub(md_pattern, f"{final_filepath}", the_saved_text)
            else:
                QMessageBox.warning(None, "Warning", "No audio found")
                ##############################

################################## If photo added move it to correct path
        # if photo is inside .enc.GitDiarySync directory ignore that file .
        md_image_pattern = re.compile(r'!\[.*\]\((?!.*\.enc\.GitDiarySync)(.*?)/([^/]+\.(png|jpg|jpeg|gif|svg|webp|bmp|tiff|ico))\)')

        matchs = md_image_pattern.findall(the_saved_text)



        for match in matchs:
            # Check if a match is found and print file location and name
            if match:
                file_location = match[0]  # match.group(1)
                file_name = match[1]  #  match.group(2)
                current_time = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                WhereTheImageIs = f"{file_location}/{file_name}"

                file_name, file_extension = file_name.rsplit('.', 1)  #  for changing filename
                new_file_name = f"{file_name}-{current_time}.{file_extension}"

                write_path = os.path.dirname(selected_file)  #  text file location
                # all_files = os.listdir(file_dir) # all other files like image

                print("============", new_file_name)
                final_filepath = copy_file_to_directoryand_rename_file(WhereTheImageIs, write_path, new_file_name)
                final_enc_filepath = final_filepath + ".enc.img.GitDiarySync"
                start_FileEncryptor("encrypt_file", final_filepath, final_enc_filepath, passwd)
                final_filepath = final_filepath.replace("\\", "/")
                final_enc_filepath = final_enc_filepath.replace("\\", "/")
                Photo_List.append(final_enc_filepath)
                md_pattern = re.escape(match[0]) + r"/" + re.escape(match[1])
                the_saved_text = re.sub(md_pattern, f"{final_filepath}", the_saved_text)
            else:
                print("No image found")



###############################################
        if filetoeditor.save:
            if edit_mode:
                last_modified_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                last_modified_date_string = f"\n\nLast modified: {last_modified_date}\n"
                total_note = f"{the_saved_text} {last_modified_date_string}"
                enc_note = start_var_data_encryptor("Enc_Fernet_PBKDF2_HMAC_SHA512", total_note, passwd)


                ##################
                Database_Lieutenant_Path = file_dir + '/' + 'Database_Lieutenant.json'
                if os.path.exists(Database_Lieutenant_Path):
                    original_data = read_json(Database_Lieutenant_Path)
                    data = original_data

                    target_path_to_find = selected_file

                    Database_Lieutenant_Path = Database_Lieutenant_Path.replace("\\", "/")
                    target_path_to_find = target_path_to_find.replace("\\", "/")

                    page_number_found = find_page_number(Database_Lieutenant_Path, target_path_to_find)
                    print(page_number_found, Database_Lieutenant_Path, target_path_to_find)

                    if page_number_found is not None:
                        print(f"{page_number_found}")
                    else:
                        print(f"No page found for the given path.")

                    Page = page_number_found #"Page" + str(check_int)
                    print(page_number_found)

                    modify_and_save_json(Database_Lieutenant_Path, original_data)

                    print("JSON file modified and saved successfully.")
                    file_name = os.path.basename(selected_file)
                    print("selected file ", selected_file)


                    Main_Name_Full_Path = selected_file

                    Main_Name_Full_Path = Main_Name_Full_Path.replace("\\", "/")

                    Main_Name = file_name
                    entry_file_path = os.path.join(file_dir, file_name)
                    write_path_Full_Path = entry_file_path


                    Database_Lieutenant_Path = file_dir + '/' + 'Database_Lieutenant.json'
                    now = datetime.now()
                    edit_end_time = now.strftime("%H:%M:%S")
                    last_entry_date =last_modified_date


                    clear_and_input_new_data(data, Page ,
                                            Main_Name, Main_Name_Full_Path,
                                            edit_start_time, edit_end_time,
                                            last_entry_date, Photo_List, Audio_List,
                                        Database_Lieutenant_Path)

                else:
                    print("error")


                with open(temp_decrypted_file, "w") as file:
                    file.write(enc_note)
                copy_file(temp_decrypted_file, selected_file)

        secure_delete_file(temp_decrypted_file)


def commit_to_git():
    if not os.path.exists('.git'):
        print("git manager")
        import gitmanager
        gitmanager.main()
    else:
        git_commands()

def input_passwd(FirstTime):
    global app
    if not app:
        app = QApplication(sys.argv)   #  Create the QApplication instance
        print("Create the QApplication instance")
    if app:
        app = QApplication.instance()  # retrieves the instance
        print("retrieves the instance")
    #app = QApplication([])
    window = QMainWindow()
    window.setWindowTitle("Personal Diary")

    window.setWindowFlag(Qt.WindowType.WindowStaysOnTopHint)
    window.setWindowFlags(Qt.Dialog)

    screen_resolution = app.primaryScreen().geometry()
    #screen_resolution = app.desktop().screenGeometry()

    width, height = 400, 200
    x = (screen_resolution.width() - width) // 2
    y = (screen_resolution.height() - height) // 2

    window.setGeometry(x, y, width, height)
    central_widget = QWidget()
    window.setCentralWidget(central_widget)
    layout = QVBoxLayout(central_widget)

    title_label = QLabel("Welcome to Personal Diary")
    title_label.setFont(QFont("Arial", 18, QFont.Bold))
    title_label.setStyleSheet("color: blue;")
    layout.addWidget(title_label)

    passwd_label = QLabel("Enter Password:")
    passwd_label.setFont(QFont("Arial", 14, QFont.Bold))
    layout.addWidget(passwd_label)

    passwd_entry = QLineEdit()
    passwd_entry.setEchoMode(QLineEdit.Password)
    passwd_entry.setFont(QFont("Arial", 14, QFont.Bold))
    layout.addWidget(passwd_entry)

    check_button = QPushButton("Unlock Diary")
    check_button.setFont(QFont("Arial", 14, QFont.Bold))
    layout.addWidget(check_button)

    #entered_passwd = ""

    def on_button_click():
        passwd = passwd_entry.text()
        window.close()
        if FirstTime:
            hash_note = HashPasswdAuthenticator("BcryptEnc", passwd, "NoHash")
            print(hash_note)
            with open("enc.GitDiarySync", 'w') as file:
                file.write(hash_note)
            msg_box = QMessageBox()
            msg_box.setText("Setup completed. Start the program again.")
            msg_box.exec()
            sys.exit()
        else:
            pass


    check_button.clicked.connect(on_button_click)
    passwd_entry.returnPressed.connect(on_button_click)
    window.show()
    app.exec()

    # passwd = passwd_entry.text()
    return passwd_entry.text()


def input_pass_now_first_time(wel_root):

    wel_root.hide()
    FirstTime = True
    input_passwd(FirstTime)

def first_time_welcome_screen():
    global app
    app = QApplication(sys.argv)
    wel_root = QMainWindow()
    wel_root.setWindowTitle("Git-backed-diary Password Verification")

    central_widget = QWidget()
    layout = QVBoxLayout()

    welcome_label = QLabel()
    welcome_label.setText("Welcome to the Git-backed diary application!\n\nFor security reasons, please set a strong password for your diary.\nKeep in mind that once set, the password cannot be recovered, so be sure to remember it.\nIf you ever wish to reset the password, delete the 'enc.GitDiarySync' file and run this program again to create a new one.\n Keep your password safe and secure, as it will protect your diary entries from unauthorized access.\n\n Tips: If you remove the 'enc.GitDiarySync' and set your old password again, you can access your old content. \n\nPlease enter your password")
    layout.addWidget(welcome_label)

    wel_root.setWindowFlag(Qt.WindowType.WindowStaysOnTopHint)
    wel_root.setWindowFlags(Qt.Dialog)

    start_button = QPushButton("Ready for input password")
    start_button.clicked.connect(lambda: input_pass_now_first_time(wel_root))
    layout.addWidget(start_button)

    central_widget.setLayout(layout)
    wel_root.setCentralWidget(central_widget)
    wel_root.show()
    sys.exit(app.exec())

def close_window(window):
    window.destroy()


def main():
    global app
    app = None
    if not os.path.exists("enc.GitDiarySync"):
        first_time_welcome_screen()
    print("\n\nWelcome to the Git-backed-diary application!\n")
    FirstTime = False
    passwd = input_passwd(FirstTime)
    print("\n")
    copy_file("enc.GitDiarySync", ".tmp")
    with open(".tmp", 'r') as file:
        encrypted_data = file.read()

    # login password check section  QMessageBox()
    content = HashPasswdAuthenticator("BcryptDec", passwd, encrypted_data)  # you can also try Pbkdf2tEnc
    if content:
        close_timer = QTimer()
        success_message = QMessageBox()
        success_message.setWindowTitle("File Content")
        success_message.setText("Your password is correct. Great!")
        success_message.setIcon(QMessageBox.Information)
        success_message.setStandardButtons(QMessageBox.Ok)
        close_timer.timeout.connect(success_message.accept)
        close_timer.start(2000)
        success_message.exec()

    else:

        error_message = QMessageBox()
        error_message.setIcon(QMessageBox.Critical)
        error_message.setWindowTitle("Password Error")
        error_message.setText("Your password appears to be incorrect.\n"
                            "If you wish to reset it, delete the 'enc.GitDiarySync' file and run this program again to create a new one.\n"
                            "Please note that you will lose access to your old notes if you set a new password.\n"
                            "However, if you can recall your old correct password, you can regain access to your files.\n"
                            "Tips: If you remove the 'enc.GitDiarySync' and set your old password again, you can access your old content.")
        error_message.exec()
        sys.exit()
    while True:
        edit_n_view_mode(passwd)

        #edit_n_view_mode(passwd, edit_mode=False)

        if platform.system() == "Linux":
            print("Great, You are a linux user!")
if __name__ == "__main__":
    main()
