import shutil
import tkinter as tk
from tkinter import messagebox
import os
import subprocess
import shutil

def create_repository(platform, username, repo_name):
    # Check if .git directory exists
    if not os.path.exists('.git'):
        initialize_git_repository()
    
    # Check if the repository already exists remotely
    remote_url = f"https://{platform}.com/{username}/{repo_name}.git"
    try:
        subprocess.run(['git', 'ls-remote', remote_url], check=True)
        show_error_with_fork_option(f"The repository '{repo_name}' already exists on {platform}. Would you like to fork it?")
    except subprocess.CalledProcessError:
        # Repository does not exist remotely, proceed with adding remote
        subprocess.run(['git', 'remote', 'add', 'origin', remote_url])
        subprocess.run(['git', 'push', '--set-upstream', 'origin', 'master'])
        messagebox.showinfo("Success", f"Repository '{repo_name}' created on {platform} successfully!")

def initialize_git_repository():
    subprocess.run(['git', 'init'])
    subprocess.run(['git', 'add', '.'])
    subprocess.run(['git', 'commit', '-m', '"Initial commit"'])

def fork_repository(platform, username, repo_name):
    

    import shutil

    # Remove existing .git directory
    shutil.rmtree('.git', ignore_errors=True)
    print("a")
    # Clone the repository

    remote_url = f"https://{platform}.com/{username}/{repo_name}.git"
    print("ab")
    subprocess.run(['git', 'clone', remote_url])
    # Clone the repository into a temporary directory
    #subprocess.run(['git', 'clone', remote_url, 'temp_directory'])
    print("ac")
    # Move all contents from the temporary directory to the current directory
    ###################
    import os
    import random
    import string

    def generate_random_string(length):
        return ''.join(random.choices(string.ascii_letters + string.digits, k=length))

    git_path = '.git'
    if os.path.exists(git_path):
        random_suffix = generate_random_string(10)
        new_name = f"remove_this_file_{random_suffix}"
        os.rename(git_path, new_name)
        print(f".git directory has been renamed to '{new_name}'")
    else:
        print(".git directory not found.")



#################
    print("acx")
    import os
    import shutil


    # Move all files and directories inside repo_name to the current directory
    for item in os.listdir(repo_name):
        try:
            shutil.move(os.path.join(repo_name, item), '.', copy_function=shutil.copy2)
        except shutil.Error:
            os.replace(os.path.join(repo_name, item), item)

    # Move hidden files and directories (starting with .) from repo_name to the current directory
    for item in os.listdir(repo_name):
        if item.startswith('.'):
            try:
                shutil.move(os.path.join(repo_name, item), '.', copy_function=shutil.copy2)
            except shutil.Error:
                os.replace(os.path.join(repo_name, item), item)

    # Remove the temporary directory
    shutil.rmtree(repo_name)


    #+++++++++++++++++++

    # Define the directory pattern to ignore
    pattern = "remove_this_file*"

    # Check if .gitignore exists, if not, create it
    gitignore_path = ".gitignore"
    if not os.path.exists(gitignore_path):
        with open(gitignore_path, 'w'):
            pass

    # Check if the pattern already exists in .gitignore
    with open(gitignore_path, 'r') as gitignore_file:
        if any(pattern in line for line in gitignore_file):
            print("Pattern already exists in .gitignore")
        else:
            # Add the pattern to .gitignore
            with open(gitignore_path, 'a') as gitignore_file:
                gitignore_file.write(pattern + "\n")
                print("Pattern added to .gitignore")


#+++++++++++++++++++
def show_error_with_fork_option(message):
    response = messagebox.askquestion("Error", message, icon='error')
    if response == 'yes':
        # Implement 'Just Fork It' functionality
        fork_repository(platform.lower(), username, repo_name)

def main():

    # Check if .git directory exists
    if os.path.exists('.git'):
        return
    root = tk.Tk()
    root.title("Git Repository Creator")

    platform_var = tk.StringVar(root)
    platform_var.set("GitLab")  # default platform

    tk.Label(root, text="Select platform:").grid(row=0, column=0)
    platform_option = tk.OptionMenu(root, platform_var, "GitHub", "GitLab")
    platform_option.grid(row=0, column=1)

    tk.Label(root, text="Username:").grid(row=1, column=0)
    username_entry = tk.Entry(root)
    username_entry.grid(row=1, column=1)

    tk.Label(root, text="New Repo Name:").grid(row=2, column=0)
    repo_entry = tk.Entry(root)
    repo_entry.grid(row=2, column=1)

    def create_repo():
        global platform, username, repo_name
        platform = platform_var.get()
        username = username_entry.get()
        repo_name = repo_entry.get()

        if not username or not repo_name:
            messagebox.showerror("Error", "Please enter username and repository name.")
        else:
            create_repository(platform.lower(), username, repo_name)

    tk.Button(root, text="Create Repository", command=create_repo).grid(row=3, columnspan=2)

    root.mainloop()

if __name__ == "__main__":
    main()
