from PySide6.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout

def on_toggled(checked, button):
    if checked:
        button.setStyleSheet("background-color: red")
        button.setText("On")
    else:
        button.setStyleSheet("background-color: green")
        button.setText("Off")

app = QApplication([])

window = QWidget()

button = QPushButton('Off')
button.setCheckable(True)
button.setStyleSheet("background-color: green")

button.toggled.connect(lambda checked: on_toggled(checked, button))

layout = QVBoxLayout()
layout.addWidget(button)
window.setLayout(layout)

window.show()
app.exec()

